﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Util;
using Android.Locations;
using System.Timers;
using Newtonsoft.Json;


namespace sparkplug
{
	
	public class MainActivity : Activity, ILocationListener
	{
		TextView addtext, loctext, distext;
		Location currentloc;
		LocationManager locationmanager;
		string locationprovider, json;
		Button send;
		private static double lat, theta, distance;
		ListView routelistview;
		private static List<route> routelist;
		private static WebClient routeclient;
		private Uri routeurlrecent;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it

			addtext = FindViewById<TextView> (Resource.Id.addresstext);
			loctext = FindViewById<TextView> (Resource.Id.locationtext);
			distext = FindViewById<TextView> (Resource.Id.distancetext);
			send = FindViewById<Button> (Resource.Id.button1);
			locationmanager = (LocationManager)GetSystemService (LocationService);

			Criteria locationServiceCriteria = new Criteria {
				Accuracy = Accuracy.Coarse,
				PowerRequirement = Power.Medium
			};

			IList<string> acceptableLocationProviders = locationmanager.GetProviders(locationServiceCriteria, true);

			if(acceptableLocationProviders.Any())
				locationprovider = acceptableLocationProviders.First();
			else
				locationprovider = String.Empty;
			
			send.Click += (sender, e) => {

				WebClient webclient = new WebClient();
				Uri uri = new Uri("http://192.168.43.95/senddata.php");

				NameValueCollection parameters = new NameValueCollection ();

				parameters.Add ("loc_id", "1");
				parameters.Add ("longtitude", lat.ToString() );
				parameters.Add ("latitude", theta.ToString());
				parameters.Add ("distance",distance.ToString() );

				webclient.UploadValuesAsync (uri, parameters);
				webclient.UploadValuesCompleted += client_UploadValuesCompleted;
			};

			Timer mytimer = new Timer ();
			mytimer.Elapsed += new ElapsedEventHandler (myfunc);
			mytimer.Interval = 500; // .5 secs
			mytimer.Enabled = true;


			routeclient = new WebClient ();
			routeurlrecent = new Uri ("http://192.168.43.2/get_route.php"); 
			int re = Checkconn (routeclient.ToString());

			if (re == 1) {

				routeclient.DownloadDataAsync (routeurlrecent);

				routeclient.DownloadDataCompleted += routeclient_DownloadDataCompleted; 
			} else if (re == 0) {
				printmoto ("No internetconnection");
			}
		}

			private int Checkconn(String url)
			{
				WebRequest webRequest = WebRequest.Create(url);  
				WebResponse webResponse;
				try 
				{
					webResponse = webRequest.GetResponse();
				}
				catch //If exception thrown then couldn't get response from address
				{	
					return 0;
				} 
				return 1;
			}

			private void printmoto(String cc)
			{
				Toast.MakeText(this, cc, ToastLength.Short).Show();
			}

		private void myfunc (object source, ElapsedEventArgs e){
			WebClient updateclient = new WebClient();
			Uri uri = new Uri("http://192.168.43.95/updatedata.php");

			NameValueCollection parameters = new NameValueCollection ();

			parameters.Add ("loc_id", "1");
			parameters.Add ("longtitude", lat.ToString() );
			parameters.Add ("latitude", theta.ToString());
			parameters.Add ("distance",distance.ToString() );

			updateclient.UploadValuesAsync (uri, parameters);
			updateclient.UploadValuesCompleted += client_UploadValuesCompleted;
		}

		private void  routeclient_DownloadDataCompleted (object sender, DownloadDataCompletedEventArgs d)
		{

			this.RunOnUiThread (() => {
				json	= Encoding.UTF8.GetString (d.Result);
				routelist = JsonConvert.DeserializeObject<List<route>> (json);

				docadap (routelist, routelistview);

			});

		}
		private void docadap(List<route> cc, ListView ee)
		{
			TaskListAdapter adapter = new TaskListAdapter(this, cc);


			ee.Adapter = adapter;
		}


		void client_UploadValuesCompleted(object sender, UploadValuesCompletedEventArgs e)
		{
			RunOnUiThread(() =>
				{
					Console.WriteLine(Encoding.UTF8.GetString(e.Result));
				});
			
		}

		protected override void OnResume() {
			base.OnResume ();


			locationmanager.RequestLocationUpdates (locationprovider, 0, 0, this);

		}

		protected override void OnPause() {
			base.OnPause ();
			locationmanager.RemoveUpdates (this);
		}

		public void OnLocationChanged(Location location){
			try{
				currentloc = location;

				if(currentloc == null){
					loctext.Text = "Location not found";
				}
				else{
					loctext.Text = String.Format("{0},{1}", 
						currentloc.Latitude, currentloc.Longitude);

					Android.Locations.Geocoder geocoder = new Geocoder(this);

					IList<Address>  addressList = 
						geocoder.GetFromLocation(currentloc.Latitude, currentloc.Longitude,5);

					Address address = addressList.FirstOrDefault();

					double lat = currentloc.Latitude;
					double theta = currentloc.Longitude - (-0.13);
					double distance = Math.Sin(Math.PI / 180.0 * (lat))
						* Math.Sin(Math.PI / 180.0 * (51.50)) +
						Math.Cos(Math.PI / 180.0 * (lat)) * 
						Math.Cos(Math.PI / 180.0 * (51.50)) *
						Math.Cos(Math.PI / 180.0 * (theta));
					distance = Math.Acos(distance);
					distance = distance/Math.PI * 180.0;
					distance = distance * 60 * 1.15;

					distext.Text = "Distance: " + distance.ToString() + "miles" ;


					if (address != null){
						StringBuilder deviceAddress =  new StringBuilder();

						for(int i = 0;  i<address.MaxAddressLineIndex; i++){
							deviceAddress.Append(address.GetAddressLine(i)).AppendLine(",");
						}

						addtext.Text = deviceAddress.ToString();

					}
					else{
						addtext.Text = "address not found";
					}
				}
			} catch {
				addtext.Text = "address not found"; 
			}

		}

		public void OnStatusChanged(string provider, Availability status, Bundle extras){

		}

		public void OnProviderDisabled(string provider){

		}

		public void OnProviderEnabled(string provider){

		}
	}
}


