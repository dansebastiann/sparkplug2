﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace sparkplug
{
	[Activity (Label = "RouteList_adapter")]			
	/// <summary>
	/// Adapter that presents Tasks in a row-view
	/// </summary>
	public class TaskListAdapter : BaseAdapter<route>
	{
		Activity context = null;
		IList<route> tasks = new List<route>();

		public TaskListAdapter(Activity context, IList<route> tasks) : base()
		{
			this.context = context;
			this.tasks = tasks;
		}

		public override route this[int position]
		{
			get { return tasks[position]; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override int Count
		{
			get { return tasks.Count; }
		}

		public override Android.Views.View GetView(int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			// Get our object for position
			var item = tasks[position];

			//Try to reuse convertView if it's not  null, otherwise inflate it from our item layout
			// gives us some performance gains by not always inflating a new view
			// will sound familiar to MonoTouch developers with UITableViewCell.DequeueReusableCell()
			var view = (convertView ??
				context.LayoutInflater.Inflate(
					Resource.Layout.RouteList,
					parent,
					false)) as LinearLayout;

			// Find references to each subview in the list item's view
			var button = view.FindViewById<TextView>(Resource.Id.button1);
			var route = view.FindViewById<TextView>(Resource.Id.route);
			var desc = view.FindViewById<TextView> (Resource.Id.description);

			//Assign item's values to the various subviews

			route.SetText(item.routename, TextView.BufferType.Normal);
			desc.SetText(item.description, TextView.BufferType.Normal);

			//Finally return the view
			return view;
		}
	}
}

