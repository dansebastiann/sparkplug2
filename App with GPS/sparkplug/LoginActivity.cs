﻿
using System;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Newtonsoft.Json;
namespace sparkplug
{
		[Activity(Label = "App2", MainLauncher = true)]
		public class LoginActivity : Activity
		{
			private static WebClient mclient;
			private static Uri murl;
			private static List<credential> credentials;
			private static string user,pass,json;

			private int Check_connection( Uri cc){
				WebRequest wreq = WebRequest.Create (cc);
				WebResponse wresp;
				try{
					wresp = wreq.GetResponse();
				} catch {
					return 0;
				}
				return 1;
			}

			private void  credential_DownloadDataCompleted (object sender, DownloadDataCompletedEventArgs e)
			{
				this.RunOnUiThread(() =>
					{
						json	= Encoding.UTF8.GetString(e.Result);
						credentials = JsonConvert.DeserializeObject<List<credential>>(json);
					});
			}
			private static String get_user()
			{
				return user;
			}
			private static String get_pass()
			{
				return pass;
			}
			private static void set_user(String s)
			{
				user = s;
			}
			private static void set_pass(String s)
			{
				pass = s;
			}
			private void print(String cc)
			{
				Toast.MakeText(this, cc, ToastLength.Short).Show();
			}

			protected override void OnCreate(Bundle bundle)
			{
				base.OnCreate (bundle);
				ActionBar.SetDisplayShowCustomEnabled (true);
				SetContentView (Resource.Layout.Login);

				//Initialized the fields & button
				Button button = FindViewById<Button> (Resource.Id.button1);
				EditText username = FindViewById<EditText> (Resource.Id.user);
				EditText password = FindViewById<EditText> (Resource.Id.pass);

				credentials = new List<credential>();
				mclient = new WebClient ();
				murl = new Uri ("http://192.168.43.2/getuserpw.php"); 
				int re = Check_connection (murl);

				if (re == 1) {
					//Retrieve credentials
					mclient.DownloadDataAsync (murl);
					mclient.DownloadDataCompleted += credential_DownloadDataCompleted; 
					//Login Button Function
					button.Click += (sender, e) => {
						//Get the value in text field
						set_user (username.Text);	
						set_pass (password.Text);
						//Authenticate the users
						if (credentials.Select (c => c.username).Contains (get_user ()) && credentials.Select (d => d.password).Contains (get_pass ())) {
							//proceed to next activity
							var intent = new Intent (this, typeof(MainActivity));
							StartActivity (intent);

						} else {

							print ("Invalid Username/Password");
							set_user ("");
							set_pass ("");
						}							
					};
				}
				else if(re == 0) {
					print("No connection");
				}

			}
		}
}

