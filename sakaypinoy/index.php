
<!DOCTYPE html>
<html>
  <head>
    <title>SparkPlug HackUP Mock Site </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap Directories -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/neat-blue.css" media="screen" id="neat-stylesheet">

    <link rel="stylesheet" href="css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">
    <link href="css/theme-switcher.css" rel="stylesheet" type="text/css">
	
	<!-- Para sa Slider Icons -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- For Google Like font -->
    <link href="css/css.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="background-clouds">
    
    <div id="nav-wrapper" class="background-white color-black">
      <nav id="mainMenu" class="navbar navbar-fixed-top" role="navigation">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar background-lead"></span>
              <span class="icon-bar background-lead"></span>
              <span class="icon-bar background-lead"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/SakayPinoy Logo.png" alt="logo">Sakay Pinoy</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="active" ><a href=""><span class="fa fa-home"></span> Home</a></li>
			  <li><a href="aboutus.php"><span class="fa fa-question-circle"></span> About us</a></li>
              <li><a href="destination.php"><span class="fa fa-rub"></span> Destination Services</a></li>
			  <li><a href="map.php"><span class="fa fa-map"></span> Current Location</a></li>
			  <li><a href="bus.php"><span class="fa fa-bus"></span> Bus Services</a></li>
              <!--<li><a href="contact.php"><span class="fa fa-book"></span> Contact</a></li>-->
			</ul>
          </div><!-- /.navbar-collapse -->
        </div>
      </nav>
    </div>

      <!-- Responsive slider - START -->
      <div class="responsive-slider-parallax" data-spy="responsive-slider" data-parallax="true" data-parallax-direction="-1" data-transitiontime="1000" data-autoplay="true">
        <div class="slides-container" data-group="slides">
          <ul>
            <li>
              <div class="slide-body" data-group="slide">
                <div class="container">
                  <div class="wrapper">
                    <div class="caption header" data-animate="slideAppearRightToLeft" data-delay="0" data-length="300">
                      <h2><span class="color-alizarin" style="font-weight: 300">Welcome to Sakay Pinoy</span></h2>
                      <p class="sub text-left" data-animate="slideAppearLeftToRight" data-delay="300" data-length="300"><span>A new way to make Commuting Easier</span></p>
                      <p class="sub text-left" data-animate="slideAppearLeftToRight" data-delay="500" data-length="300"><span>Affordable</span></p>
                      <p class="sub text-left" data-animate="slideAppearLeftToRight" data-delay="700" data-length="300"><span>Clean and modern design</span></p>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="slide-body" data-group="slide">
                <div class="container">
                  <div class="wrapper">
                    <div class="caption header text-center" data-animate="slideAppearUpToDown" data-delay="0" data-length="300">
                      <h2><span class="color-carrot" style="font-weight: 300">Over 25 Stop points</span></h2>
                      <p class="sub" data-animate="slideAppearDownToUp" data-delay="300" data-length="300"><span>Designated on high priority locations</span></p>
                      <p class="sub" data-animate="slideAppearDownToUp" data-delay="500" data-length="300"><span>Safe </span></p>
                      <p class="sub" data-animate="slideAppearDownToUp" data-delay="700" data-length="300"><span>With multi-progress report</span></p>
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="slide-body" data-group="slide">
                <div class="container">
                  <div class="wrapper">
                    <div class="caption header text-right" data-animate="slideAppearDownToUp" data-delay="0" data-length="300">
                      <h2><span style="font-weight: 300"><span class="color-alizarin"> Put</span><span class="color-carrot"> Your</span><span class="color-sun-flower"> ID</span><span class="color-emerald"> to</span><span class="color-peter-river"> the</span><span class="color-alizarin"> scanner</h2>
                      <p class="sub" data-animate="slideAppearUpToDown" data-delay="300" data-length="300"><span>For faster transaction</span></p>
                      <p class="sub" data-animate="slideAppearUpToDown" data-delay="500" data-length="300"><span>Go to Destination Services Tab</span></p>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <a class="slider-control left hidden-xs hidden-sm" href="#" data-jump="prev"><i class="fa fa-angle-left"></i></a>
        <a class="slider-control right hidden-xs hidden-sm" href="#" data-jump="next"><i class="fa fa-angle-right"></i></a>
        <div class="pages-wrapper background-black">
          <ol class="pages">
            <li><a data-jump-to="1">1</a></li>
            <li><a data-jump-to="2">2</a></li>
            <li><a data-jump-to="3">3</a></li>
          </ol>
        </div>
      </div>
      <!-- Responsive slider - END -->
    </div>

    <footer class="background-midnight-blue color-white" data-control="class">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <h3>Menu</h3>
            <ul class="nav-footer">
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="aboutus.php">About us</a></li>
              <li><a href="destination.php">Destination Services</a></li>
              <li><a href="map.php">Map</a></li>
              <li><a href="bus.php">Bus Services</a></li>
            </ul>
          </div>
          <div class="col-md-4">
            <h3>Testmonials</h3>
            <p class="testimonial">It Entails the discipline of the Filipino Commuters.</p>
            <p class="author">Gab</p>
            <p class="testimonial">The service is far more better than expected.</p>
            <p class="author">Ace</p>
			<p class="testimonial">Two Thumbs up.</p>
            <p class="author">Ervin</p>
          </div>
          <div class="col-md-5">
            <h3>Contact us</h3>
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label for="inputName1" class="col-lg-3 control-label">Name</label>
                <div class="col-lg-9">
                  <input type="text" class="form-control input-lg" id="inputName1" placeholder="Name">
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail1" class="col-lg-3 control-label">Email</label>
                <div class="col-lg-9">
                  <input type="email" class="form-control input-lg" id="inputEmail1" placeholder="Email">
                </div>
              </div>
              <div class="form-group">
                <label for="inputContent1" class="col-lg-3 control-label">Content</label>
                <div class="col-lg-9">
                  <textarea id="inputContent1" class="form-control" rows="3"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-3 col-lg-9">
                  <button type="submit" class="btn btn-lead btn-lg">Send us an email</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
            <div class="row">
              <div class="col-md-6">
                All rights reserved &copy; 2015 <a href="http://SparkPlug.com">SparkPlug.com</a>
              </div>
              <div class="col-md-6">
                <p class="social">
                  <a href="/"><span class="fa fa-facebook"></span></a>
                  <a href="/"><span class="fa fa-twitter"></span></a>
                  <a href="/"><span class="fa fa-youtube"></span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
    <div id="blueimp-gallery" class="blueimp-gallery">
      <!-- The container for the modal slides -->
      <div class="slides"></div>
      <!-- Controls for the borderless lightbox -->
      <h3 class="title">title</h3>
      <a class="prev">‹</a>
      <a class="next">›</a>
      <a class="close">×</a>
      <a class="play-pause"></a>
      <ol class="indicator"></ol>
      <!-- The modal dialog, which will be used to wrap the lightbox content -->
      <div class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" aria-hidden="true">&times;</button>
              <h4 class="modal-title">title</h4>
            </div>
            <div class="modal-body next"></div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                Previous
              </button>
              <button type="button" class="btn btn-primary next">
                Next
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Include slider -->
    <script src="js/jquery.event.move.js"></script>
    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/responsive-calendar.js"></script>
    <script src="js/jquery.blueimp-gallery.min.js"></script>
    <script src="js/bootstrap-image-gallery.min.js"></script>
    <script src="js/reduce-menu.js"></script>
    <script src="js/match-height.js"></script>
    <script type="text/javascript">
    function getUrlParameter(sParam)
    {
      var sPageURL = window.location.search.substring(1);
      var sURLVariables = sPageURL.split('&');
      for (var i = 0; i < sURLVariables.length; i++) 
      {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
          return sParameterName[1];
        }
      }
      return 0;
    }  
    $(document).ready(function() {
      $('[data-switch]').on('click', function(){
        $('#neat-stylesheet').attr('href', 'css/neat-'+ $(this).data('switch') +'.css');
      });

      $('[data-nav]').on('click', function(){
        $('#nav-wrapper').attr('class', $(this).data('nav'));
      });

      var colorPicker = '';
      colorPicker += '<div class="theme-switcher text-left hidden background-white color-black">';
      colorPicker += '  <h3>Change element color</h3>';
      colorPicker += '  <p>For most elements, background and text colors can be set using simple color classes (eg. "background-alizarin color-white"). You can find background class for every <a href="http://flatuicolors.com">flat ui color</a>.</p><p>Please tryout this few examples:</p>';
      colorPicker += '  <a data-pick="background-white color-black" title="background-white color-black" class="background-white"><span>Light gray</span></a>';
      colorPicker += '  <a data-pick="background-clouds color-black" title="background-clouds color-black" class="background-clouds"><span>Light gray</span></a>';
      colorPicker += '  <a data-pick="background-belize-hole color-white" title="background-belize-hole color-white" class="background-belize-hole"><span>Blue</span></a>';
      colorPicker += '  <a data-pick="background-alizarin color-white" title="background-alizarin color-white" class="background-alizarin"><span>Red</span></a>';
      colorPicker += '  <a data-pick="background-pomegranate color-white" title="background-pomegranate color-white" class="background-pomegranate"><span>Red</span></a>';
      colorPicker += '  <a data-pick="background-nephritis color-white" title="background-nephritis color-white" class="background-nephritis"><span>Green</span></a>';
      colorPicker += '  <a data-pick="background-pumpkin color-white" title="background-pumpkin color-white" class="background-pumpkin"><span>Orange</span></a>';
      colorPicker += '  <a data-pick="background-wisteria color-white" title="background-wisteria color-white" class="background-wisteria"><span>Purple</span></a>';
      colorPicker += '  <a data-pick="background-midnight-blue color-white" title="background-midnight-blue color-white" class="background-midnight-blue"><span>Dark blue</span></a>';
      colorPicker += '  <a data-pick="background-turquoise color-white" title="background-turquoise color-white" class="background-turquoise"><span>Turquise</span></a>';
      colorPicker += '  <a data-pick="background-green-sea color-white" title="background-green-sea color-white" class="background-green-sea"><span>Turquise darker</span></a>';
      colorPicker += '</div>';

      $('[data-control="class"]').css('position', 'relative').append($('<div data-control="cog"><a style="cursor: pointer"><span class="fa fa-cog"></span></a></div>').css({
        'position': 'absolute',
        'top': '15px',
        'right': '15px',
        'text-align': 'right'
      })).on('mouseover', function(){ $(this).children('[data-control="cog"]').css('display', 'block') }).on('mouseout', function(){ $(this).children('[data-control="cog"]').css('display', 'none') });

      $('[data-control="class"] [data-control="cog"]').append(colorPicker);

      $('[data-control="class"] [data-control="cog"] > a').on('click', function(){
        $(this).parent().children('div').toggleClass('hidden');
        return false;
      });

      $('[data-pick]').on('click', function(){
        var target = $(this).parent().parent().parent();
        target.attr('class', target.data('class-prefix')+' '+$(this).data('pick'));
        return false;
      });

      var boxed = getUrlParameter('boxed');
      $('[name="layout-style"][value="'+boxed+'"]').attr('checked', 'checked');

      if (boxed == 1) {
        $.each($('nav a[href*=".html"]'), function(i, item){
          $(this).attr('href', $(this).attr('href')+'?boxed=1');
        });
      }

      $('[name="layout-style"]').on('change', function() {
        window.location.href = '?boxed='+$(this).val();
      });

      //$('[data-pick]').tooltip();
    });
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
      $('.responsive-calendar').responsiveCalendar({
        time: '2013-05',
        events: {
          "2013-05-30": {"number": 5, "badgeClass": "background-turquoise", "url": "http://w3widgets.com/responsive-slider"},
          "2013-05-26": {"number": 1, "badgeClass": "background-turquoise", "url": "http://w3widgets.com"}, 
          "2013-05-03": {"number": 1, "badgeClass": "background-pomegranate"}, 
          "2013-05-12": {}}
      });

      $('#mixit').mixitup();
    });

    $(window).load(function(){
      if ($(window).width() > 767) {
        matchHeight($('.info-thumbnail .caption .description'), 3);
      }

      $(window).on('resize', function(){
        if ($(window).width() > 767) {
          $('.info-thumbnail .caption .description').height('auto');
          matchHeight($('.info-thumbnail .caption .description'), 3);
        }
      });
    });
    </script>
  </body>
</html>