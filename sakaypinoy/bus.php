<html>
  <head>
    <title>SparkPlug HackUP Mock Site </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap Directories -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/neat-blue.css" media="screen" id="neat-stylesheet">

    <link rel="stylesheet" href="css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">
    <link href="css/theme-switcher.css" rel="stylesheet" type="text/css">
	
	<script src="http://js.api.here.com/v3/3.0/mapsjs-core.js" type="text/javascript"></script>
	<script src="http://js.api.here.com/v3/3.0/mapsjs-service.js" type="text/javascript"></script>
	<script src="http://js.api.here.com/v3/3.0/mapsjs-mapevents.js" type="text/javascript"></script>
	<script src="http://js.api.here.com/v3/3.0/mapsjs-ui.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="http://js.api.here.com/v3/3.0/mapsjs-ui.css">
	
	<!-- Para sa Slider Icons / Glyphicon Alternative Font Awesome -->
	<link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- For google like font -->
    <link href="css/css.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	
	
	<!-- map Icons -->
	<style type="text/css">
	.directions li span.arrow {
	  display:inline-block;
	  min-width:28px;
	  min-height:28px;
	  background-position:0px;
	  background-image: url("../img/arrows.png");
	  position:relative;
	  top:8px;
	}
	.directions li span.depart  {
	  background-position:-28px;
	}
	.directions li span.rightUTurn  {
	  background-position:-56px;
	}
	.directions li span.leftUTurn  {
	  background-position:-84px;
	}
	.directions li span.rightFork  {
	  background-position:-112px;
	}
	.directions li span.leftFork  {
	  background-position:-140px;
	}
	.directions li span.rightMerge  {
	  background-position:-112px;
	}
	.directions li span.leftMerge  {
	  background-position:-140px;
	}
	.directions li span.slightRightTurn  {
	  background-position:-168px;
	}
	.directions li span.slightLeftTurn{
	  background-position:-196px;
	}
	.directions li span.rightTurn  {
	  background-position:-224px;
	}
	.directions li span.leftTurn{
	  background-position:-252px;
	}
	.directions li span.sharpRightTurn  {
	  background-position:-280px;
	}
	.directions li span.sharpLeftTurn{
	  background-position:-308px;
	}
	.directions li span.rightRoundaboutExit1 {
	  background-position:-616px;
	}
	.directions li span.rightRoundaboutExit2 {
	  background-position:-644px;
	}

	.directions li span.rightRoundaboutExit3 {
	  background-position:-672px;
	}

	.directions li span.rightRoundaboutExit4 {
	  background-position:-700px;
	}

	.directions li span.rightRoundaboutPass {
	  background-position:-700px;
	}

	.directions li span.rightRoundaboutExit5 {
	  background-position:-728px;
	}
	.directions li span.rightRoundaboutExit6 {
	  background-position:-756px;
	}
	.directions li span.rightRoundaboutExit7 {
	  background-position:-784px;
	}
	.directions li span.rightRoundaboutExit8 {
	  background-position:-812px;
	}
	.directions li span.rightRoundaboutExit9 {
	  background-position:-840px;
	}
	.directions li span.rightRoundaboutExit10 {
	  background-position:-868px;
	}
	.directions li span.rightRoundaboutExit11 {
	  background-position:896px;
	}
	.directions li span.rightRoundaboutExit12 {
	  background-position:924px;
	}
	.directions li span.leftRoundaboutExit1  {
	  background-position:-952px;
	}
	.directions li span.leftRoundaboutExit2  {
	  background-position:-980px;
	}
	.directions li span.leftRoundaboutExit3  {
	  background-position:-1008px;
	}
	.directions li span.leftRoundaboutExit4  {
	  background-position:-1036px;
	}
	.directions li span.leftRoundaboutPass {
	  background-position:1036px;
	}
	.directions li span.leftRoundaboutExit5  {
	  background-position:-1064px;
	}
	.directions li span.leftRoundaboutExit6  {
	  background-position:-1092px;
	}
	.directions li span.leftRoundaboutExit7  {
	  background-position:-1120px;
	}
	.directions li span.leftRoundaboutExit8  {
	  background-position:-1148px;
	}
	.directions li span.leftRoundaboutExit9  {
	  background-position:-1176px;
	}
	.directions li span.leftRoundaboutExit10  {
	  background-position:-1204px;
	}
	.directions li span.leftRoundaboutExit11  {
	  background-position:-1232px;
	}
	.directions li span.leftRoundaboutExit12  {
	  background-position:-1260px;
	}
	.directions li span.arrive  {
	  background-position:-1288px;
	}
	.directions li span.leftRamp  {
	  background-position:-392px;
	}
	.directions li span.rightRamp  {
	  background-position:-420px;
	}
	.directions li span.leftExit  {
	  background-position:-448px;
	}
	.directions li span.rightExit  {
	  background-position:-476px;
	}

	.directions li span.ferry  {
	  background-position:-1316px;
	</style>
	
	
	
	
	
	
	
	
	
	
  </head>
  <body class="background-clouds">
	<div id="nav-wrapper" class="background-white color-black">
      <nav id="mainMenu" class="navbar navbar-fixed-top" role="navigation">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar background-lead"></span>
              <span class="icon-bar background-lead"></span>
              <span class="icon-bar background-lead"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/SakayPinoy Logo.png" alt="logo">Sakay Pinoy</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="index.php"><span class="fa fa-home"></span> Home</a></li>
			  <li><a href="aboutus.php"><span class="fa fa-question-circle"></span> About us</a></li>
              <li><a href="destination.php"><span class="fa fa-rub"></span> Destination Services</a></li>
			  <li><a href="map.php"><span class="fa fa-map"></span> Current Location</a></li>
			  <li class="active"><a href="/"><span class="fa fa-map"></span> Bus Services</a></li>
			</ul>
          </div><!-- /.navbar-collapse -->
        </div>
      </nav>
    </div>
    
    <!-- Breadcrumbs - START -->
    <div class="breadcrumbs-container">
      <div class="container">
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li class="active">Maps</li>
        </ol>
      </div>
    </div>
    <!-- Breadcrumbs - END -->
	
	<section class="">
      <div class="container">
		
		<div class="col-md-12"></div>
		
		<h1><span>Progress Report</span></h1>
		<div class="col-md-6">
		 <div id="map" style="width:100%; height:100%; background:grey" >
		 <span style="font-size:30px">Current Passengers: 14</span></br>
		 <span style="font-size:30px">Next Station: SM Megamall</span></br>
		 <span style="font-size:30px">Passengers departing at next station: 3</span></br>
		 </div>
		</div>
		<div class="col-md-6">
		 <div id="panel" style="width:100%; left:51%; height:100%; background:inherit" >
		 <span style="font-size:30px">Total Pending Passengers: 14</span></br>
		 <span style="font-size:30px">Next Recieving Passengers: 5</span></br>
		 </div>
		</div>
		  
		  
		  
	   
		</div>
		</div>
	<!-- End of Pricing-->
	
	<footer class="background-midnight-blue color-white" data-control="class">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <h3>Menu</h3>
            <ul class="nav-footer">
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="aboutus.php">About us</a></li>
              <li><a href="destination.php">Destination Services</a></li>
              <li><a href="map.php">Map</a></li>
              <li><a href="bus.php">Bus Services</a></li>
            </ul>
          </div>
          <div class="col-md-4">
            <h3>Testmonials</h3>
            <p class="testimonial">It Entails the discipline of the Filipino Commuters.</p>
            <p class="author">Gab</p>
            <p class="testimonial">The service is far more better than expected.</p>
            <p class="author">Ace</p>
			<p class="testimonial">Two Thumbs up.</p>
            <p class="author">Ervin</p>
          </div>
          <div class="col-md-5">
            <h3>Contact us</h3>
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label for="inputName1" class="col-lg-3 control-label">Name</label>
                <div class="col-lg-9">
                  <input type="text" class="form-control input-lg" id="inputName1" placeholder="Name">
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail1" class="col-lg-3 control-label">Email</label>
                <div class="col-lg-9">
                  <input type="email" class="form-control input-lg" id="inputEmail1" placeholder="Email">
                </div>
              </div>
              <div class="form-group">
                <label for="inputContent1" class="col-lg-3 control-label">Content</label>
                <div class="col-lg-9">
                  <textarea id="inputContent1" class="form-control" rows="3"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-lg-offset-3 col-lg-9">
                  <button type="submit" class="btn btn-lead btn-lg">Send us an email</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <hr>
            <div class="row">
              <div class="col-md-6">
                All rights reserved &copy; 2015 <a href="http://SparkPlug.com">SparkPlug.com</a>
              </div>
              <div class="col-md-6">
                <p class="social">
                  <a href="/"><span class="fa fa-facebook"></span></a>
                  <a href="/"><span class="fa fa-twitter"></span></a>
                  <a href="/"><span class="fa fa-youtube"></span></a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
    <div id="blueimp-gallery" class="blueimp-gallery">
      <!-- The container for the modal slides -->
      <div class="slides"></div>
      <!-- Controls for the borderless lightbox -->
      <h3 class="title">title</h3>
      <a class="prev">‹</a>
      <a class="next">›</a>
      <a class="close">×</a>
      <a class="play-pause"></a>
      <ol class="indicator"></ol>
      <!-- The modal dialog, which will be used to wrap the lightbox content -->
      <div class="modal fade">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" aria-hidden="true">&times;</button>
              <h4 class="modal-title">title</h4>
            </div>
            <div class="modal-body next"></div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left prev">
                <i class="glyphicon glyphicon-chevron-left"></i>
                Previous
              </button>
              <button type="button" class="btn btn-primary next">
                Next
                <i class="glyphicon glyphicon-chevron-right"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Include slider -->
    <script src="js/jquery.event.move.js"></script>
    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/responsive-calendar.js"></script>
    <script src="js/jquery.blueimp-gallery.min.js"></script>
    <script src="js/bootstrap-image-gallery.min.js"></script>
    <script src="js/reduce-menu.js"></script>
    <script src="js/match-height.js"></script>
    <script type="text/javascript">
    function getUrlParameter(sParam)
    {
      var sPageURL = window.location.search.substring(1);
      var sURLVariables = sPageURL.split('&');
      for (var i = 0; i < sURLVariables.length; i++) 
      {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
          return sParameterName[1];
        }
      }
      return 0;
    }  
    $(document).ready(function() {
      $('[data-switch]').on('click', function(){
        $('#neat-stylesheet').attr('href', 'css/neat-'+ $(this).data('switch') +'.css');
      });

      $('[data-nav]').on('click', function(){
        $('#nav-wrapper').attr('class', $(this).data('nav'));
      });

      var colorPicker = '';
      colorPicker += '<div class="theme-switcher text-left hidden background-white color-black">';
      colorPicker += '  <h3>Change element color</h3>';
      colorPicker += '  <p>For most elements, background and text colors can be set using simple color classes (eg. "background-alizarin color-white"). You can find background class for every <a href="http://flatuicolors.com">flat ui color</a>.</p><p>Please tryout this few examples:</p>';
      colorPicker += '  <a data-pick="background-white color-black" title="background-white color-black" class="background-white"><span>Light gray</span></a>';
      colorPicker += '  <a data-pick="background-clouds color-black" title="background-clouds color-black" class="background-clouds"><span>Light gray</span></a>';
      colorPicker += '  <a data-pick="background-belize-hole color-white" title="background-belize-hole color-white" class="background-belize-hole"><span>Blue</span></a>';
      colorPicker += '  <a data-pick="background-alizarin color-white" title="background-alizarin color-white" class="background-alizarin"><span>Red</span></a>';
      colorPicker += '  <a data-pick="background-pomegranate color-white" title="background-pomegranate color-white" class="background-pomegranate"><span>Red</span></a>';
      colorPicker += '  <a data-pick="background-nephritis color-white" title="background-nephritis color-white" class="background-nephritis"><span>Green</span></a>';
      colorPicker += '  <a data-pick="background-pumpkin color-white" title="background-pumpkin color-white" class="background-pumpkin"><span>Orange</span></a>';
      colorPicker += '  <a data-pick="background-wisteria color-white" title="background-wisteria color-white" class="background-wisteria"><span>Purple</span></a>';
      colorPicker += '  <a data-pick="background-midnight-blue color-white" title="background-midnight-blue color-white" class="background-midnight-blue"><span>Dark blue</span></a>';
      colorPicker += '  <a data-pick="background-turquoise color-white" title="background-turquoise color-white" class="background-turquoise"><span>Turquise</span></a>';
      colorPicker += '  <a data-pick="background-green-sea color-white" title="background-green-sea color-white" class="background-green-sea"><span>Turquise darker</span></a>';
      colorPicker += '</div>';

      $('[data-control="class"]').css('position', 'relative').append($('<div data-control="cog"><a style="cursor: pointer"><span class="fa fa-cog"></span></a></div>').css({
        'position': 'absolute',
        'top': '15px',
        'right': '15px',
        'text-align': 'right'
      })).on('mouseover', function(){ $(this).children('[data-control="cog"]').css('display', 'block') }).on('mouseout', function(){ $(this).children('[data-control="cog"]').css('display', 'none') });

      $('[data-control="class"] [data-control="cog"]').append(colorPicker);

      $('[data-control="class"] [data-control="cog"] > a').on('click', function(){
        $(this).parent().children('div').toggleClass('hidden');
        return false;
      });

      $('[data-pick]').on('click', function(){
        var target = $(this).parent().parent().parent();
        target.attr('class', target.data('class-prefix')+' '+$(this).data('pick'));
        return false;
      });

      var boxed = getUrlParameter('boxed');
      $('[name="layout-style"][value="'+boxed+'"]').attr('checked', 'checked');

      if (boxed == 1) {
        $.each($('nav a[href*=".html"]'), function(i, item){
          $(this).attr('href', $(this).attr('href')+'?boxed=1');
        });
      }

      $('[name="layout-style"]').on('change', function() {
        window.location.href = '?boxed='+$(this).val();
      });

      //$('[data-pick]').tooltip();
    });
    </script>

    <script type="text/javascript">
    $(window).load(function(){
      matchHeight($('.info-thumbnail .caption .description'), 4);
    });
    </script>
	
	<script type="text/javascript">
			var platform = new H.service.Platform(
				{
					//Here is Here maps Developer API ID and Key
					'app_id': '4zNf9m80fqHODke6gaRn',
					'app_code': 's_eZcYAy-35zvsd3jJwG5A'
				});

			var defaultLayers = platform.createDefaultLayers();

			var position = {lat: 14.6568 , lng: 121.0739};

			var map = new H.Map(
				document.getElementById('mapContainer'), 
				defaultLayers.normal.map, 
				{
					zoom: 15, 
					center: position
			});

			// Enable event system on the map instance:
			var mapEvents = new H.mapevents.MapEvents(map);

			var behavior = new H.mapevents.Behavior(mapEvents);

			var ui = H.ui.UI.createDefault(map, defaultLayers);

			var marker = new H.map.Marker(position);
			map.addObject(marker);
		</script>
	

	
			
	
	
  </body>
</html>