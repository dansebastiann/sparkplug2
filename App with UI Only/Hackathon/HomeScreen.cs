﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;
using Android.Support.V4.App;
using Android.Locations;

namespace Hackathon
{
	[Activity (Label = "HomeScreen")]			
	public class HomeScreen : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.HomeScreen);
			Button AddTripbtn = FindViewById<Button>(Resource.Id.addbutton);
			Button showbus = FindViewById<Button> (Resource.Id.showbuses);

			AddTripbtn.Click += (object sender, EventArgs e) => {
				var addtripactivity = new Intent (this, typeof(addtrip));
				StartActivity (addtripactivity);
			};
		}
	}
}

