﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Hackathon
{
	[Activity (Label = "Hackathon", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		/*public void checkFieldsForEmptyValues(object sender, Android.Text.AfterTextChangedEventArgs e)
		{
			Button button = FindViewById<Button>(Resource.Id.loginButton);

			//String s1 = EditText.getText().toString();
			EditText username_form = FindViewById<EditText>(Resource.Id.username);
			string _username = username_form.Text;
			EditText password_form = FindViewById<EditText>(Resource.Id.password);
			string _password = password_form.Text;

			if (_username.Equals("") && _password.Equals(""))
			{
				button.Enabled = false;
			}

			else if (!_username.Equals("") && _password.Equals(""))
			{
				button.Enabled = false;
			}
			else if (_username.Equals("") && !_password.Equals(""))
			{
				button.Enabled = false;
			}
			else
			{
				button.Enabled = true;
			}
		}*/
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.Main);
			EditText usernametxt = FindViewById<EditText> (Resource.Id.username);
			EditText passwordtxt = FindViewById<EditText> (Resource.Id.password);
			Button loginbtn = FindViewById<Button> (Resource.Id.loginButton);

			/*
			loginbtn.Enabled = false;
			usernametxt.AfterTextChanged += checkFieldsForEmptyValues;
			passwordtxt.AfterTextChanged += checkFieldsForEmptyValues;
			*/

			loginbtn.Click += (object sender, EventArgs e) => {
				var homescreenactivity = new Intent (this, typeof(HomeScreen));
				StartActivity (homescreenactivity);
			};

		}
	}
}


