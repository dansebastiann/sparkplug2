﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Hackathon
{
	[Activity (Label = "addtrip")]			
	public class addtrip : Activity
	{
		EditText destination_name;
		Button submit_button;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Addtrip);
			destination_name = FindViewById<EditText> (Resource.Id.txtdestinationdetail);
			submit_button = FindViewById<Button> (Resource.Id.confirmbutton);
			submit_button.Click += btn_submit_click;  
		}
		void btn_submit_click(object sender, EventArgs e){

			if(destination_name.Text!="")  
			{  
				//ing the Activity2 in Intent  
				Intent i = new Intent(this,typeof(selectrideactivity));  
				//Add PutExtra method data to intent.  
				i.PutExtra("destination",destination_name.Text.ToString());  
				//StartActivity  
				StartActivity(i);  
			}  
			else  
			{  
				Toast.MakeText(this,"Please Provide Destination",ToastLength.Short).Show();  
			}  
		}
	}
}

