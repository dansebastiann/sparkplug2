﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Hackathon
{
	[Activity (Label = "selectrideactivity")]			
	public class selectrideactivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.selectride);
			TextView confirmeddestination = FindViewById<TextView> (Resource.Id.txtconfirmeddestination);
			string a = Intent.GetStringExtra ("destination");

			confirmeddestination.Text = a;
		}
	}
}

